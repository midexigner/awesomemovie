import React from 'react'
import { StyleSheet,SafeAreaView } from 'react-native'

const SafeBox = props => {
    return <SafeAreaView {...props} style={{ ...styles.droidSafeArea, ...props.style }} />
}

export default SafeBox

const styles = StyleSheet.create({
    droidSafeArea: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? 25 : 0,
        backgroundColor:'#fff'
      }
})
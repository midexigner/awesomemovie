import React from 'react'
import { View, Text } from 'react-native'
//tabs bottom
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Feather,AntDesign } from '@expo/vector-icons';
import HomeStack from './HomeStack';
import Profile from '../screens/Profile';
import Notification from '../screens/Notification';
import Setting from '../screens/Setting';

export default function TabStack({navigation}) {
// tabs initail
const Tab = createBottomTabNavigator();
    return (
        <Tab.Navigator
       tabBarOptions={{
        activeTintColor: 'white',
        inactiveTintColor: 'gray',
        style: {
          backgroundColor: '#030303'
          },
          labelStyle: {
            fontSize: 13,
        },
      }}
      >
        <Tab.Screen 
        name="Home" 
        component={HomeStack}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: () => (
            <AntDesign name="home" size={24} color="white" />
          ),
        }}
        />
        <Tab.Screen 
        name="Settings" 
        component={Setting} 
        options={{
          tabBarLabel: 'Setting',
          tabBarIcon: () => (
            <AntDesign name="setting" size={24} color="white" />
          ),
        }}
        />
        <Tab.Screen 
        name="Notification" 
        component={Notification}
        options={{
          tabBarLabel: 'Notification',
          tabBarIcon: () => (
            <AntDesign name="notification" size={24} color="white" />
          ),
        }}
        />
        <Tab.Screen 
        name="Profile" 
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: () => (
            <AntDesign name="user" size={24} color="white" />
          ),
        }}
        />
      </Tab.Navigator>
    )
}

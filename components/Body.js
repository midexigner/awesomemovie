import React from 'react'
import { StyleSheet,View } from 'react-native'
import Color from '../constant/colors'
const Body = props => {
    return <View {...props} style={{ ...styles.container, ...props.style }} />
}

export default Body

const styles = StyleSheet.create({
    container: {
            flex: 1,
            backgroundColor: Color.accent,
            paddingHorizontal:20
       
      }
})
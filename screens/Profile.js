import React from 'react'
import { View, Text } from 'react-native'
import Body from '../components/Body'
import Header from '../components/Header'
import SafeBox from '../components/SafeBox'

export default function Profile({navigation}) {
    return (
        <SafeBox>
             <Header title="Profile" navigation={navigation} />
             <Body>
           <Text style={{color:'#fff'}}>Profile Screen</Text>
           </Body>
        </SafeBox>
    )
}

import React from 'react'
import {Text } from 'react-native'
import Body from '../components/Body'
import Header from '../components/Header'
import SafeBox from '../components/SafeBox'

export default function Setting({navigation}) {
    return (
        <SafeBox>
             <Header title="Settings" navigation={navigation} />
            <Body>
           <Text style={{color:'#fff'}}>Setting Screen</Text>
           </Body>
        </SafeBox>
    )
}

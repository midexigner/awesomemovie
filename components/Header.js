import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import { Feather,Entypo} from '@expo/vector-icons'; 
import Color from '../constant/colors'

export default function Header({title,navigation}) {
    return (
        <View style={styles.screen}>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.headerRight}>
            <TouchableOpacity onPress={() => alert('Search!')}>
            <Feather name="search" size={24} color="#a6a6a6" />
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> navigation.openDrawer()}>
            <Entypo name="dots-three-vertical" size={24} color="#a6a6a6" />
            </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen:{
        height:90,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:24,
        backgroundColor: Color.accent
    },
    title:{
        fontSize:22,
        fontWeight:'700',
        color:'#fff'
    },
    headerRight:{
        flexDirection:'row',
        alignItems:'center',
    }
})

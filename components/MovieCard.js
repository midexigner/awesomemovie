import React from 'react'
import { StyleSheet, Text, View,Image } from 'react-native'

const MovieCard = (props) => {
    const {title,pic} = props;
    return (
        <View  style={{marginHorizontal:10,width:125}}>
             <Image source={{uri:pic}}
            style={{
              width:125,
              height:180,
            }}
            resizeMode="cover"
            />
            <Text style={{color:"#929292",paddingTop:10,paddingBottom:15}}>{title}</Text>
        </View>
    )
}

export default MovieCard

const styles = StyleSheet.create({})

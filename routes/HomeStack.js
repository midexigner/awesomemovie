import React from 'react'
import Home from '../screens/Home'
import Profile from '../screens/Profile';
// Stack 
import { createStackNavigator } from '@react-navigation/stack';


export default function HomeStack({navigation}) {
    // Stack nav
const Stack = createStackNavigator();

    const navOptionHandler = ()=>({
        headerShown:false
      })

    return (
        <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} options={navOptionHandler} />
      <Stack.Screen name="Profile" component={Profile}  options={navOptionHandler} />
    </Stack.Navigator>
    )
}

import React from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import requests from '../services/requests'
import Header from '../components/Header';
import Row from '../components/Row';
import SafeBox from '../components/SafeBox';
import Body from '../components/Body';


export default function Home({navigation}) {
  
  return (
    <SafeBox style={styles.container}>
      <Header title="My Feed"  navigation={navigation}  />
    <Body>
     <ScrollView>
     <Row title='Trending Now' fetchUrl={requests.fetchTrending} />
     <Row title='Mystery' fetchUrl={requests.fetchMystery} />
     <Row title='Series' fetchUrl={requests.fetchTv} />
     <Row title='Animation' fetchUrl={requests.fetchAnimation} />
      <Row title="Action Movies" fetchUrl={requests.fetchActionMovies} />
      <Row title="Comedy Movies" fetchUrl={requests.fetchComedyMovies} />
      <Row title="Horror Movies" fetchUrl={requests.fetchHorrorMovies} />
      <Row title="Romance Movies" fetchUrl={requests.fetchRomanceMovies} />
     </ScrollView>
     </Body>
    </SafeBox>
  );
}

const styles = StyleSheet.create({});

import React from 'react'
import { View, Text } from 'react-native'
import Body from '../components/Body'
import Header from '../components/Header'
import SafeBox from '../components/SafeBox'

export default function Notification({navigation}) {
    return (
        <SafeBox>
             <Header title="Notification" navigation={navigation} />
             <Body>
           <Text style={{color:'#fff'}}>Notification Screen</Text>
           </Body>
        </SafeBox>
    )
}

import React, { useState } from 'react'
import { View, Text,StyleSheet, TextInput,Image,FlatList,TouchableHighlight,Modal } from 'react-native'
import axios from 'axios';

const Search = () => {
  const apiurl = "http://www.omdbapi.com/?apikey=a06040ed"
  const [state, setState] = useState({
    s:"Enter a movie...",//Enter a movie...
    results:[],
    selected:{}
  });
  const search = () =>{
    axios(apiurl + "&s=" + state.s).then(({data})=>{
      let results = data.Search;
    
      setState(prevState =>{
        return {...prevState,results:results}
      })
    })
  }
  const openPopup = (id) =>{
    axios(apiurl + "&i=" + id).then(({data})=>{

      let results = data;
      setState(prevState =>{
        return {...prevState,selected:results}
      });
   
    });

  }
  return (
    <View style={styles.container}>
       <Text style={styles.title}>Movie DB</Text>
       <TextInput onChangeText={text => setState(prevState =>{
         return {...prevState,s: text}
       })}
       onSubmitEditing={search} 
       style={styles.searchBox} value={state.s} />
<FlatList 
       horizontal
        keyExtractor={(item) => item.imdbID} 
        data={state.results} 
        renderItem={({ item }) => ( 
          <TouchableHighlight key={item.imdbID}  onPress={() => openPopup(item.imdbID)}>
          <View style={styles.result}>
            <Image source={{uri:item.Poster}}
            style={{
              width:150,
              height:150,
            }}
            resizeMode="cover"
            />
            <Text style={styles.heading} numberOfLines={2} ellipsizeMode="tail">{item.Title}</Text>
            </View>
            </TouchableHighlight>
        )}
      />

       <Modal
       animationType="slide"
       transparent={false}
       visible={(typeof state.selected.Title !='undefined')}
       >
      <View> 
        <Text>{state.selected.Title}</Text>
        <Text style={{marginBottom:20}}>Rating: {state.selected.imdbRating}</Text>
        <Text style={{marginBottom:20}}>{state.selected.Plot}</Text>
        <TouchableHighlight style={styles.closeBtn} onPress={() => setState(prevState =>{ return { ...prevState,selected:{}}})}><Text>Closed</Text></TouchableHighlight>
      </View>
     
       </Modal>
      </View>
  )
}

export default Search;

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#111111',
    alignItems:'center',
    justifyContent:'flex-start',
    paddingTop:70,
    paddingHorizontal:20
  },title:{
    color:'#fff',
    fontSize:32,
    fontWeight:'700',
    textAlign:'center',
    marginBottom:20
  },
  searchBox:{
    fontSize:20,
    fontWeight: '300',
    padding: 20,
    width: '100%',
    backgroundColor: '#fff',
    borderRadius:8,
    marginBottom:40
  },
  results:{
    flex:1,
  },
  result:{
    flex:1,
    width:'100%',
    marginBottom:20
  },
  heading:{
    color:'#878787',
    fontSize:14,
    fontWeight:'700',
    padding:10,
    backgroundColor:'#445565'
  },
  popup:{
    padding:20
  },
  poptitle:{
    fontSize:24,
    fontWeight:'700',
    marginBottom:5
  },
  closeBtn:{
    padding:20,
    fontSize:20,
    fontWeight:'700',
    backgroundColor:'#2484c4'
  }
});

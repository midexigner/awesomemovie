import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './screens/Login';
import DrawerStack from './routes/DrawerStack';

export default function App({navigation}) {
  const navOptionHandler = ()=>({
    headerShown:false
  })
  const StackApp = createStackNavigator(); 
  return (
    <NavigationContainer>
      <StackApp.Navigator initialRouteName="Login">
      <StackApp.Screen name="Home" component={DrawerStack} options={navOptionHandler} />
      <StackApp.Screen name="Login" component={Login}  options={navOptionHandler} />
    </StackApp.Navigator>
    </NavigationContainer>
  )
}

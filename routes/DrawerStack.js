import React from 'react'
import { View, Text } from 'react-native'
// drawer
import { createDrawerNavigator } from '@react-navigation/drawer';
import TabStack from './TabStack';
import Notification from '../screens/Notification';
import Setting from '../screens/Setting';

export default function DrawerStack({navigation}) {
const Drawer = createDrawerNavigator();
    return (
        <Drawer.Navigator initialRouteName="Home"
    drawerStyle={{
      backgroundColor: '#c6cbef',
      width: 240,
    }}
    >
     <Drawer.Screen name="Home" component={TabStack} />
     <Drawer.Screen name="Notification" component={Notification} />
     <Drawer.Screen name="Settings" component={Setting} />
   </Drawer.Navigator>
    )
}

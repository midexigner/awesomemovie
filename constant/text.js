export default {
    headingOne: 36,
    headingTwo: 30,
    headingThree: 24,
    headingFour: 18,
    headingFive: 16,
    headingSix: 14,
  };
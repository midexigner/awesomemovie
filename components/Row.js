import React, { useState,useEffect } from "react";
import { StyleSheet, Text, View, TouchableOpacity,FlatList } from "react-native";
import { Entypo } from "@expo/vector-icons";
import axios from '../services/axios';

import MovieCard from "./MovieCard";

const base_url = "https://image.tmdb.org/t/p/original/"
const Row = (props) => {
    const [movies, setMovies] = useState([]);
    const { title,fetchUrl } = props;
    useEffect(()=>{
        async function fetchData() {
            const request = await axios.get(fetchUrl);
            setMovies(request.data.results);
            return request;
            }
            fetchData();
       
    },[fetchUrl])
    //console.log(movies)
  return (
    <View style={styles.screen}>
      <View style={styles.headingSection}>
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity onPress={() => alert("all")}>
          <Entypo name="chevron-small-right" size={24} color="#a6a6a6" />
        </TouchableOpacity>
      </View>
      <View style={{flexDirection:'row',marginVertical:10}}>
      <FlatList 
       horizontal
       showsHorizontalScrollIndicator={false}
        keyExtractor={(movie) => movie.id} 
        data={movies.slice(0,6)} 
        maxToRenderPerBatch={6}
        initialScrollIndex={1}
        renderItem={({ item }) => (
            <MovieCard pic={`${base_url}${item.backdrop_path}`} title={item.title || item.original_name} />
        )}
      />
      {/*  */}
         {/**/}
      </View>
    </View>
  );
};

export default Row;

const styles = StyleSheet.create({
  screen: {
    paddingVertical: 5,
    paddingHorizontal: 24,
    alignContent: "center",
  },
  headingSection: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 18,
    color: "#e8e8e8",
    textTransform: "capitalize",
  },
});
